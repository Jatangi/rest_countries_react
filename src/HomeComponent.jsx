
import React, { useContext, useEffect, useState } from 'react';
import { IoSearchOutline, IoMoonOutline } from "react-icons/io5";
import { AllCards } from './components/AllCards';
import { ThemeContext } from './components/Themeprovider';
import Toast from './components/Toast';



const regionarr = ['Asia', 'Americas', 'Africa', 'Oceania', 'Europe', 'Antarctic'];

const HomeComponent = () => {

  const { darkTheme, setTheme } = useContext(ThemeContext)

  const [apiData, setApiData] = useState([]);

  const [Region, setRegion] = useState('');

  const [input, setInput] = useState('');

  const [subRegion, setSubRegion] = useState('');

  const [population, setPopulation] = useState('');

  const [area, setArea] = useState('');

  const [status, setStatus] = useState({ loading: true, error: null });

  useEffect(() => {
    async function fetchdata() {
      try {
        const data = await fetch('https://restcountries.com/v3.1/all')

        const jsondata = await data.json()
        setApiData(jsondata);
        setStatus({ loading: false, error: null });
        //console.log(status.loading)
      }
      catch (error) {
        setStatus({ loading: false, error: error.message });
      }
    }
    fetchdata()
  }, []);
  if (status.loading) {
    return (
      <div className="absolute top-0 left-0 bg-glare h-full w-full grid place-content-center">
        <div className="text-black text-3xl tracking-widest font-bold">Loading...</div>
      </div>
    );
  }

  if (status.error) {
    return <Toast value={status.error} />;
  }

  const readInput = (e) => {
    setInput(e.target.value);
  };


  function getSubregionsData() {
    const subregions = apiData.filter(country => country.region === Region).map(country => country.subregion);

    const outputArray = Array.from(new Set(subregions));

    return outputArray;
  }

  const readRegion = (e) => {

    setRegion(e.target.value);
    setSubRegion('');
  }

  


  const changeSubregion = (e) => {
    setSubRegion(e.target.value);
  };

  const sortPopulation = (e) => {
    setPopulation(e.target.value);
  };

  const sortArea = (e) => {
    setArea(e.target.value);
  };

  let filteredCountries = apiData.filter((eachCountry) => {
    if (Region !== "" || input !== "" || subRegion !== "") {
      if (eachCountry.region.toLowerCase() === Region.toLowerCase()) {
        if (subRegion === "" || eachCountry.subregion === subRegion) {
          return eachCountry.name.official.toLowerCase().includes(input.toLowerCase());
        }
      }
    }
    else {
      return eachCountry;
    }
  });

  if (population === "ascending") {
    filteredCountries.sort((a, b) => a.population - b.population);
  } else if (population === "descending") {
    filteredCountries.sort((a, b) => b.population - a.population);
  }

  if (area === "ascending") {
    filteredCountries.sort((a, b) => a.area - b.area);
  } else if (area === "descending") {
    filteredCountries.sort((a, b) => b.area - a.area);
  }

  const appClassName = darkTheme ? 'app dark bg-gray-900 text-white border-none' : 'app bg-white text-gray-900 pb-[700px]';
  const selectClassName = darkTheme ? 'float-right mr-10 mt-10 p-2 pr-5 bg-gray-700   pl-2 rounded-md shadow-md text-white' : 'float-right mr-10 mt-10 p-2 pr-5 bg-white border-2 pl-2 rounded-md shadow-md';
  const optionClassName = darkTheme ? 'bg-gray-700  text-white' : '';
  const inputClassName = darkTheme ? " p-3 pl-10 mt-10 ml-10 shadow-md text-white bg-gray-700 " : " p-3 pl-10 mt-10 ml-10 shadow-md text-black bg-white";
  
  if (!apiData) {
    return <div>Loading...</div>;
  } else {

    return (
      <div className={appClassName}>
        <header className={`py-10 shadow-md ${darkTheme ? "bg-gray-700" : ""}`}>
          <div className='float-right'>
            <IoMoonOutline className='absolute text-2xl -ml-8' />
            <div>
              <button className='mr-10 text-xl shadow-md' onClick={() => {
                console.log("-->" + darkTheme);
                setTheme(!darkTheme);
              }}>
                {darkTheme ? 'Light mode' : 'Dark mode'}
              </button>
            </div>
          </div>
          <p className=' ml-10 text-2xl py-1.5'>Where in the world?</p>
        </header>
        <div className={`${darkTheme ? "bg-gray-800" : ""}`}>
          <IoSearchOutline className='absolute mt-14 ml-12' />

          <input type="text" value={input} onChange={readInput} className={inputClassName} placeholder='Search for the country' />

          {getSubregionsData().length !== 0 && (
            <select className={selectClassName} value={subRegion} onChange={changeSubregion}>
              <option className={optionClassName} value="">Select Subregion</option>
              {getSubregionsData().map((sub, index) => (
                <option className={optionClassName} key={index} value={sub}>{sub}</option>
              ))}
            </select>
          )}

          <select className={selectClassName} value={population} onChange={sortPopulation}>
            <option className={optionClassName} value="">Sort by Population</option>
            <option className={optionClassName} value="ascending">Ascending</option>
            <option className={optionClassName} value="descending">Descending</option>
          </select>

          <select className={selectClassName} value={area} onChange={sortArea}>
            <option className={optionClassName} value="">Sort by Area</option>
            <option className={optionClassName} value="ascending">Ascending</option>
            <option className={optionClassName} value="descending">Descending</option>
          </select>

          <select className={selectClassName} value={Region} onChange={readRegion}>
            <option className={optionClassName} value="">Select Region</option>
            {regionarr.map((element, index) => (
              <option className={optionClassName} key={index} value={element}>{element}</option>
            ))}
          </select>

          <div className='flex flex-wrap mt-10 p-10'>
            {filteredCountries && filteredCountries.length > 0 ? filteredCountries.map((country, index) => (
              <AllCards key={index} data={country} darkTheme={darkTheme} />
            )) : <p className='absolute ml-[800px] text-2xl mt-52'>No matches found.................</p>}
          </div>
        </div>
      </div>
    );
  }
};

export default HomeComponent;
