import React from 'react'
import { Routes, Route } from 'react-router-dom'
import App from './HomeComponent'
import { EachCard } from './components/EachCard'
import { Homepage } from './Homepage'

export const RouterComponent = () => {
    //console.log("------------routerjsx--------");
    return (
        <Routes>
            <Route path='/' element={<Homepage/>}></Route>
            <Route path="/details/:id" element={<EachCard/>}></Route>
        </Routes>
    )
}
