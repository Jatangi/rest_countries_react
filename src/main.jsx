import React from 'react'
import ReactDOM from 'react-dom/client'
import './index.css'
import { BrowserRouter } from "react-router-dom"
import { RouterComponent } from './Router.jsx'
import ThemeProvider from './components/Themeprovider.jsx'


ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <ThemeProvider>
      <BrowserRouter>

        <RouterComponent />

      </BrowserRouter>
    </ThemeProvider>
  </React.StrictMode>,
)
