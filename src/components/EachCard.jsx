
import React, { useState, useEffect } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { IoMoonOutline } from 'react-icons/io5';
import { FaArrowLeftLong } from 'react-icons/fa6';
import Toast from './Toast';

import { ThemeContext } from './Themeprovider';
import { useContext } from 'react';

export const EachCard = () => {
  const navigate = useNavigate();
  const { id } = useParams();
  const { darkTheme, setTheme } = useContext(ThemeContext);

  const [countrydata, setCountryData] = useState([]);
  const [eachCountry, setEachCountry] = useState(null);
  const [status, setStatus] = useState({ loading: true, error: null });

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await fetch(`https://restcountries.com/v3.1/all`);
        if (!response.ok) {
          throw new Error('Failed to fetch data');
        }
        const jsondata = await response.json();
        setCountryData(jsondata);

        const countryDetail = jsondata.find(country => country.cca3 === id);
        if (countryDetail) {
          setEachCountry(countryDetail);
          setStatus({ loading: false, error: null });
        } else {
          setStatus({ loading: false, error: `Country with ID ${id} not found` });
        }
      } catch (error) {
        setStatus({ loading: false, error: error.message });
      }
    };

    fetchData();
  }, [id]); // Only run when id changes

  if (status.loading) {
    return (
      <div className="absolute top-0 left-0 bg-glare h-full w-full grid place-content-center">
        <div className="text-black text-3xl tracking-widest font-bold">Loading...</div>
      </div>
    );
  }

  if (status.error) {
    return <Toast value={status.error} />;
  }

  if (!eachCountry) {
    return <div>Loading...</div>;
  }

  return (
    <div className={`pb-60 ${darkTheme ? 'bg-gray-800 text-white border-none h-full' : 'bg-white text-gray-900'}`}>
      <header className={`py-10 shadow-md ${darkTheme ? 'bg-gray-700' : ''}`}>
        <div className='float-right'>
          <IoMoonOutline className='absolute text-2xl -ml-8' />
          <div>
            <button className='mr-10 text-xl shadow-md' onClick={() => setTheme(!darkTheme)}>
              Dark mode
            </button>
          </div>
        </div>
        <p className='ml-10 text-2xl py-1.5'>Where in the world?</p>
      </header>
      <FaArrowLeftLong className='absolute mt-14 ml-14' />
      <button
        className={`pl-20 pr-20 pt-3 pb-3 ml-10 mt-10 shadow-md ${darkTheme ? 'bg-gray-700' : ''}`}
        onClick={() => navigate(-1)}
      >
        Go back
      </button>
      <div>
        <img src={eachCountry.flags?.png} alt='' className='w-[700px] h-[50vh] ml-20 mt-52 mr-64 pb-10' />
      </div>
      <div className='flex flex-row pr-60 space-x-10 float-right -mt-[300px] justify-between pb-10'>
        <h1 className='absolute -mt-20 pl-10'><b>{eachCountry.name?.official}</b></h1>
        <div className='flex flex-col pr-10'>
          <p><b>Native name :</b> {eachCountry.name?.common}</p>
          <p><b>Population :</b> {eachCountry.population}</p>
          <p><b>Region :</b> {eachCountry.region}</p>
          <p><b>Subregion :</b> {eachCountry.subregion}</p>
          <p><b>Capital :</b> {eachCountry.capital}</p>
        </div>
        <div className='flex flex-col pr-5'>
          <p><b>Top level domain :</b> {eachCountry.tld}</p>
          <p><b>Currencies :</b> {Object.keys(eachCountry.currencies || {}).join(',')}</p>
          <p><b>Languages :</b> {Object.values(eachCountry.languages || {}).join(',')}</p>
        </div>
        <div className='absolute flex flex-row mt-60 pr-8 '>
          <p><b>Borders countries:</b></p>
          {eachCountry.borders && eachCountry.borders.length > 0 ? (
            eachCountry.borders.map(element => (
              <button
                key={element}
                className={`ml-5 pl-5 pr-5 shadow-md ${darkTheme ? 'bg-gray-700' : ''}`}
                onClick={() => navigate(`/details/${element}`)}>
                {element}
              </button>
            ))
          ) : (
            <span>No borders present</span>
          )}
        </div>
      </div>
    </div>
  );
};





