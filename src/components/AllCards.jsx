import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import { useNavigate } from 'react-router-dom'


export const AllCards = (props) => {
  const { data, darkTheme } = props
  console.log(darkTheme)
  const navigate = useNavigate();
  const handleClick = () => navigate(`/details/${data.cca3}`);


  //console.log(data)

  return (
    <>


      <div className={`card w-[15%] m-10 shadow-lg rounded-lg ${darkTheme ? "bg-gray-700 shadow-lg" : "bg-white"}`} onClick={()=>handleClick()}>
        <img src={data.flags.png} className="" alt="" />
        <h1 className='pt-10 pl-5'><b>{data.name.official}</b></h1>
        <div className='pl-5 pr-5 pb-10 pt-10'>
          <p><b>Population</b>:{data.population}</p>
          <p><b>Area</b>: {data.area}</p>
          <p><b>Region</b>:{data.region}</p>
          <p><b>Capital</b>:{data.capital}</p>
          <p><b>Subregion</b>: {data.subregion}</p>
        </div>

      </div>

    </>

  )
}
