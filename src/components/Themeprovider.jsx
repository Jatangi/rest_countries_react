
import { createContext, useState } from "react";

export const ThemeContext = createContext(null);

function ThemeProvider(props) {
  const [darkTheme, setTheme] = useState(false);

  return (
    <ThemeContext.Provider value={{ darkTheme, setTheme }}>
      {props.children}
    </ThemeContext.Provider>
  );
}

export default ThemeProvider;

