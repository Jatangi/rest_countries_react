// useTheme.js

import { useContext } from 'react';
import { ThemeContext } from './Themeprovider';


const useTheme = () => {
  const themeContext = useContext(ThemeContext);
  return themeContext;
};

export default useTheme;


